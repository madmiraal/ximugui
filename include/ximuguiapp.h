// (c) 2014: Marcel Admiraal

#ifndef XIMUGUIAPP_H
#define XIMUGUIAPP_H

#include <wx/app.h>

namespace xIMU
{
    class GUIApp : public wxApp
    {
    public:
        /**
         * Initialises the application.
         *
         * @return True to continue processing.
         */
        virtual bool OnInit();
    };
}

#endif // XIMUGUIAPP_H
