// Listens to packets received from the x-IMU
//
// (c) 2014: Marcel Admiraal

#ifndef XIMUGUILISTENER_H
#define XIMUGUILISTENER_H

#include "ximulistener.h"
#include "oscilloscopepanel.h"
#include "cuboidpanel.h"

namespace xIMU
{
    class GUIFrame;
    class GUIListener : public Listener
    {
    public:
        GUIListener(GUIFrame* frame);
        virtual void onErrorDataReceived(ErrorData);
        virtual void onCommandDataReceived(CommandData);
        virtual void onRegisterDataReceived(RegisterData);
        virtual void onDateTimeDataReceived(DateTimeData);
        virtual void onDigitalIODataReceived(DigitalIOData);
        virtual void onRawAnalogueInputDataReceived(RawAnalogueInputData);
        virtual void onCalAnalogueInputDataReceived(CalAnalogueInputData);
        virtual void onPWMOutputDataReceived(PWMOutputData);
        virtual void onRawADXL345BusDataReceived(RawADXL345BusData);
        virtual void onCalADXL345BusDataReceived(CalADXL345BusData);

    private:
        GUIFrame* guiFrame;
    };

    class VoltageDataListener : public Listener
    {
    public:
        VoltageDataListener(ma::OscilloscopePanel*);
        virtual void onCalBatteryAndTemperatureDataReceived(CalBatteryAndTemperatureData);
    private:
        ma::OscilloscopePanel* oscilloscope;
    };

    class TemperatureDataListener : public Listener
    {
    public:
        TemperatureDataListener(ma::OscilloscopePanel*);
        virtual void onCalBatteryAndTemperatureDataReceived(CalBatteryAndTemperatureData);
    private:
        ma::OscilloscopePanel* oscilloscope;
    };

    class GyroscopeDataListener : public Listener
    {
    public:
        GyroscopeDataListener(ma::OscilloscopePanel*);
        virtual void onCalInertialAndMagneticDataReceived(CalInertialAndMagneticData);
    private:
        ma::OscilloscopePanel* oscilloscope;
    };

    class AccelerometerDataListener : public Listener
    {
    public:
        AccelerometerDataListener(ma::OscilloscopePanel*);
        virtual void onCalInertialAndMagneticDataReceived(CalInertialAndMagneticData);
    private:
        ma::OscilloscopePanel* oscilloscope;
    };

    class MagnetometerDataListener : public Listener
    {
    public:
        MagnetometerDataListener(ma::OscilloscopePanel*);
        virtual void onCalInertialAndMagneticDataReceived(CalInertialAndMagneticData);
    private:
        ma::OscilloscopePanel* oscilloscope;
    };

    class QuaternionDataListener : public Listener
    {
    public:
        QuaternionDataListener(ma::CuboidPanel*);
        virtual void onQuaternionDataReceived(QuaternionData);
    private:
        ma::CuboidPanel* cuboidPanel;
    };
}

#endif // XIMUGUILISTENER_H

