// (c) 2014: Marcel Admiraal

#ifndef XIMUGUIFRAME_H
#define XIMUGUIFRAME_H

#include "ximuapi.h"
#include "ximuguilistener.h"
#include "ximuenum.h"

#include <wx/dialog.h>
#include <wx/statbmp.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/statusbr.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/statbmp.h>
#include <wx/sizer.h>
#include <wx/statbox.h>
#include <wx/panel.h>
#include <wx/treectrl.h>
#include <wx/notebook.h>
#include <wx/frame.h>
#include <wx/choice.h>
#include <wx/dialog.h>

#define idMenuQuit 1000
#define idMenuAbout 1001
#define OPEN_CLOSE_BOX 1002

namespace xIMU
{
    class GUIFrame : public wxFrame
    {
    public:
        /**
         * Constructor.
         *
         * @param parent    Pointer to the frame's parent.
         */
        GUIFrame(wxWindow* parent);

        /**
         * Default destructor.
         */
        virtual ~GUIFrame();

        /**
         * Updates the status bar.
         *
         * @param status    The new status message.
         */
        void updateStatusBar(const wxString& status);

        /**
         * Updates register data.
         *
         * @param address   The address of the register to update.
         * @param value     The new register value.
         */
        void updateRegisterData(const RegisterAddress& address,
                const float value);

        /**
         * Updates the date and time.
         *
         * @param year      The new year.
         * @param month     The new month.
         * @param day       The new day.
         * @param hour      The new hour.
         * @param minute    The new minute.
         * @param second    The new second.
         * @param value     The new register value.
         */
        void updateDateTime(
            const wxString& year,
            const wxString& month,
            const wxString& day,
            const wxString& hour,
            const wxString& minute,
            const wxString& second);

    private:
        void OnClose(wxCloseEvent &event);
        void OnQuit(wxCommandEvent &event);
        void OnAbout(wxCommandEvent &event);
        void tabbedChanged(wxNotebookEvent &event);
        void portNameChanged(wxCommandEvent &event);
        void connectPort(wxCommandEvent &event);
        void getDateTime(wxCommandEvent &event);
        void setDateTime(wxCommandEvent &event);
		void nullCommand(wxCommandEvent &event);
		void factoryResetCommand(wxCommandEvent &event);
		void softwareResetCommand(wxCommandEvent &event);
		void sleepCommand(wxCommandEvent &event);
		void sleepResetCommand(wxCommandEvent &event);
		void gyroscopeSampleAxisCommand(wxCommandEvent &event);
		void gyroscopeCalculateSensitivityCommand(wxCommandEvent &event);
		void gyroscopeBias1Command(wxCommandEvent &event);
		void gyroscopeBias2Command(wxCommandEvent &event);
		void gyroscopeCalculateBiasCommand(wxCommandEvent &event);
		void accelerometerSampleAxisCommand(wxCommandEvent &event);
		void accelerometerCalculateCommand(wxCommandEvent &event);
		void magnetometerMeasureCommand(wxCommandEvent &event);
		void algorithmInitialiseCommand(wxCommandEvent &event);
		void algorithmTareCommand(wxCommandEvent &event);
		void algorithmClearTareCommand(wxCommandEvent &event);
		void algorithmInitialiseTareCommand(wxCommandEvent &event);
		void showBatteryGraph(wxCommandEvent &event);
		void showThermometerGraph(wxCommandEvent &event);
		void showGyroscopeGraph(wxCommandEvent &event);
		void showAccelerometerGraph(wxCommandEvent &event);
		void showMagnetometerGraph(wxCommandEvent &event);
		void show3DCuboid(wxCommandEvent &event);
		void showEulerAngleGraph(wxCommandEvent &event);

		void buildRegisterTree();
		wxTreeItemId getRegisterTreeItem(
            wxTreeItemId rootItem, RegisterAddress address);

		void treeItemExpanded(wxTreeEvent &event);
		void treeItemRightClick(wxTreeEvent &event);
		static void rightClickMenuClick(wxCommandEvent &event);

        API* api;
        GUIListener* guiListener;
		wxMenuBar* menu;
		wxMenu* fileMenu;
		wxMenu* helpMenu;
		wxStatusBar* statusBar;
		wxNotebook* tabbedPanel;
		wxPanel* serialTab;
		wxStaticText* portNameTextLabel;
		wxTextCtrl* portNameText;
		wxButton* connectPortButton;
		wxStaticBitmap* connectedLight;
		wxPanel* registerTab;
		wxTreeCtrl* registerTree;
		wxPanel* dateTimeTab;

		wxStaticText* yearTextLabel;
		wxTextCtrl* yearText;

		wxStaticText* monthTextLabel;
		wxTextCtrl* monthText;

		wxStaticText* dayTextLabel;
		wxTextCtrl* dayText;


		wxStaticText* hourTextLabel;
		wxTextCtrl* hourText;

		wxStaticText* minuteTextLabel;
		wxTextCtrl* minuteText;

		wxStaticText* secondTextLabel;
		wxTextCtrl* secondText;


		wxButton* getDateTimeButton;
		wxButton* setDateTimeButton;

		wxPanel* commandTab;
		wxButton* nullButton;
		wxButton* factoryResetButton;
		wxButton* softwareResetButton;
		wxButton* sleepButton;
		wxButton* sleepResetButton;
		wxButton* gyroscopeSampleAxisButton;
		wxButton* gyroscopeCalculateSensitivityButton;
		wxButton* gyroscopeBias1Button;
		wxButton* gyroscopeBias2Button;
		wxButton* gyroscopeCalculateBiasButton;
		wxButton* accelerometerSampleAxisButton;
		wxButton* accelerometerCalculateButton;
		wxButton* magnetometerMeasureButton;
		wxButton* algorithmInitialiseButton;
		wxButton* algorithmTareButton;
		wxButton* algorithmClearTareButton;
		wxButton* algorithmInitialiseTareButton;
		wxPanel* sensorDataTab;
		wxButton* showBatteryGraphButton;
		wxButton* showThermometerGraphButton;
		wxButton* showGyroscopeGraphButton;
		wxButton* showAccelerometerGraphButton;
		wxButton* showMagnetometerGraphButton;
		wxButton* show3DCuboidButton;
		wxButton* showEulerAngleGraphButton;
    };

    class RegisterTreeItemData : public wxTreeItemData
    {
    public:
        RegisterTreeItemData(
            wxString name, RegisterAddress address,
            bool editable = false, QValue qValue = (QValue)0,
            wxString unit = wxEmptyString, bool hexValue = false);
        RegisterTreeItemData(
            wxString name, RegisterAddress address,
            wxArrayString mapValue, wxString unit = wxT(""));
        virtual ~RegisterTreeItemData();
        wxString getName();
        RegisterAddress getAddress();
        bool isValueSet();
        float getValue();
        void setValue(float value);
        bool isEditable();
        QValue getQValue();
        bool isMapped();
        wxArrayString getMapValue();
        wxString getText();
    private:
        wxString name;
        RegisterAddress address;
        float value;
        bool valueSet;
        bool editable;
        QValue qValue;
        bool mapped;
        wxArrayString mapValue;
        wxString unit;
        bool hexValue;
    };

    class EditDialog : public wxDialog
    {
    public:
        EditDialog(wxWindow* parent,
            RegisterTreeItemData* registerData,
            unsigned short* newValue);
        virtual ~EditDialog();
    private:
        float getMaxFloat(QValue qValue);
        float getMinFloat(QValue qValue);
        void okButtonPressed(wxCommandEvent &event);

        RegisterTreeItemData* registerData;
        unsigned short* newValue;
		wxStaticText* editLabel;
		wxTextCtrl* editValue;
		wxChoice* editChoice;
		wxStdDialogButtonSizer* editDialogButton;
		wxButton* editDialogButtonOK;
		wxButton* editDialogButtonCancel;
    };

    class GraphFrame: public wxFrame
    {
    public:
        GraphFrame(wxWindow* parent, API* api, const wxString& title);
        virtual ~GraphFrame();
    protected:
        API* api;
        Listener* listener;
    };

    class VoltageGraphFrame : public GraphFrame
    {
    public:
        VoltageGraphFrame(wxWindow* parent, API* api);
    };

    class TemperatureGraphFrame : public GraphFrame
    {
    public:
        TemperatureGraphFrame(wxWindow* parent, API* api);
    };

    class GyroscopeGraphFrame : public GraphFrame
    {
    public:
        GyroscopeGraphFrame(wxWindow* parent, API* api);
    };

    class AccelerometerGraphFrame : public GraphFrame
    {
    public:
        AccelerometerGraphFrame(wxWindow* parent, API* api);
    };

    class MagnetometerGraphFrame : public GraphFrame
    {
    public:
        MagnetometerGraphFrame(wxWindow* parent, API* api);
    };

    class CuboidFrame : public GraphFrame
    {
    public:
        CuboidFrame(wxWindow* parent, API* api);
    };
}

// Control IDs.
enum
{
    ID_EDIT,
    ID_REFRESH,
    ID_REFRESH_ALL,
};

#endif // XIMUGUIFRAME_H
