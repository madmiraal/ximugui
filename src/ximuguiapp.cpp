// (c) 2014: Marcel Admiraal

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "ximuguiapp.h"
#include "ximuguiframe.h"

IMPLEMENT_APP(xIMU::GUIApp);

namespace xIMU
{
    bool GUIApp::OnInit()
    {
        wxImage::AddHandler(new wxPNGHandler);
        GUIFrame* frame = new GUIFrame(0L);
        frame->Show();
        return true;
    }
}
