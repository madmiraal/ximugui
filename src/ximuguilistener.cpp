// (c) 2014: Marcel Admiraal

#include "quaternion.h"
#include "vector3d.h"
#include "ximuguilistener.h"
#include "ximuguiframe.h"

namespace xIMU
{
    GUIListener::GUIListener(GUIFrame* frame)
        : guiFrame(frame)
    {
    }

    void GUIListener::onErrorDataReceived(ErrorData data)
    {
    }

    void GUIListener::onCommandDataReceived(CommandData data)
    {
        guiFrame->updateStatusBar(wxString(data.getMessage()));
    }

    void GUIListener::onRegisterDataReceived(RegisterData data)
    {
        guiFrame->updateRegisterData(
            data.getAddress(), data.getFloatValue());
    }

    void GUIListener::onDateTimeDataReceived(DateTimeData data)
    {
        guiFrame->updateDateTime(
            wxString::Format(wxT("%i"),data.getYear()),
            wxString::Format(wxT("%i"),data.getMonth()),
            wxString::Format(wxT("%i"),data.getDay()),
            wxString::Format(wxT("%i"),data.getHour()),
            wxString::Format(wxT("%i"),data.getMinute()),
            wxString::Format(wxT("%i"),data.getSecond())
        );
    }

    void GUIListener::onDigitalIODataReceived(DigitalIOData data)
    {
    }

    void GUIListener::onRawAnalogueInputDataReceived(RawAnalogueInputData data)
    {
    }

    void GUIListener::onCalAnalogueInputDataReceived(CalAnalogueInputData data)
    {
    }

    void GUIListener::onPWMOutputDataReceived(PWMOutputData data)
    {
    }

    void GUIListener::onRawADXL345BusDataReceived(RawADXL345BusData data)
    {
    }

    void GUIListener::onCalADXL345BusDataReceived(CalADXL345BusData data)
    {
    }

    VoltageDataListener::VoltageDataListener(ma::OscilloscopePanel* oscilloscope) :
        oscilloscope(oscilloscope)
    {
    }

    void VoltageDataListener::onCalBatteryAndTemperatureDataReceived(CalBatteryAndTemperatureData data)
    {
        oscilloscope->addData(data.getBatteryVoltage());
    }

    TemperatureDataListener::TemperatureDataListener(ma::OscilloscopePanel* oscilloscope) :
        oscilloscope(oscilloscope)
    {
    }

    void TemperatureDataListener::onCalBatteryAndTemperatureDataReceived(CalBatteryAndTemperatureData data)
    {
        oscilloscope->addData(data.getTemperature());
    }

    GyroscopeDataListener::GyroscopeDataListener(ma::OscilloscopePanel* oscilloscope) :
        oscilloscope(oscilloscope)
    {
    }

    void GyroscopeDataListener::onCalInertialAndMagneticDataReceived(CalInertialAndMagneticData data)
    {
        float gyroscopeFloatData[3];
        data.getGyroscope(gyroscopeFloatData);
        ma::Vector gyroscopeData(gyroscopeFloatData, 3);
        oscilloscope->addData(gyroscopeData);
    }

    AccelerometerDataListener::AccelerometerDataListener(ma::OscilloscopePanel* oscilloscope) :
        oscilloscope(oscilloscope)
    {
    }

    void AccelerometerDataListener::onCalInertialAndMagneticDataReceived(CalInertialAndMagneticData data)
    {
        float accelerometerFloatData[3];
        data.getAccelerometer(accelerometerFloatData);
        ma::Vector accelerometerData(accelerometerFloatData, 3);
        oscilloscope->addData(accelerometerData);
    }

    MagnetometerDataListener::MagnetometerDataListener(ma::OscilloscopePanel* oscilloscope) :
        oscilloscope(oscilloscope)
    {
    }

    void MagnetometerDataListener::onCalInertialAndMagneticDataReceived(CalInertialAndMagneticData data)
    {
        float magnetometerFloatData[3];
        data.getMagnetometer(magnetometerFloatData);
        ma::Vector magnetometerData(magnetometerFloatData, 3);
        oscilloscope->addData(magnetometerData);
    }

    QuaternionDataListener::QuaternionDataListener(ma::CuboidPanel* cuboidPanel) :
        cuboidPanel(cuboidPanel)
    {
    }

    void QuaternionDataListener::onQuaternionDataReceived(QuaternionData data)
    {
        float quaternionFloatData[4];
        data.getQuaternion(quaternionFloatData);
        ma::Quaternion orientation(
                (double)quaternionFloatData[0],
                (double)quaternionFloatData[1],
                (double)quaternionFloatData[2],
                (double)quaternionFloatData[3]);
        cuboidPanel->setOrientation(orientation);
    }
}
