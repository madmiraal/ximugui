// (c) 2014: Marcel Admiraal

#include "ximuguiframe.h"

#include <wx/aboutdlg.h>
#include <wx/msgdlg.h>
#include <wx/menu.h>
#include <wx/textctrl.h>
#include <wx/treectrl.h>
#include <wx/window.h>
#include <wx/statbmp.h>

#include "ximuguiicons.h"
#include "oscilloscopepanel.h"
#include "vector3dpanel.h"

#include <sstream>

namespace xIMU
{
    GUIFrame::GUIFrame(wxWindow* parent)
        : wxFrame(parent, wxID_ANY, wxT("xIMU GUI"), wxDefaultPosition,
            wxSize(600, 466), wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL)
    {
        SetIcon(wxIcon(wxT("imu.ico")));

        SetSizeHints( wxDefaultSize, wxDefaultSize );

        menu = new wxMenuBar();

        fileMenu = new wxMenu();
        wxMenuItem* menuFileQuit;
        menuFileQuit = new wxMenuItem(fileMenu, idMenuQuit, wxT("&Quit\tAlt+F4"),
                wxT("Quit the application"), wxITEM_NORMAL);
        fileMenu->Append(menuFileQuit);
        menu->Append( fileMenu, wxT("&File") );

        helpMenu = new wxMenu();
        wxMenuItem* menuHelpAbout;
        menuHelpAbout = new wxMenuItem(helpMenu, idMenuAbout, wxT("&About\tF1"),
                wxT("Show info about this application"), wxITEM_NORMAL);
        helpMenu->Append(menuHelpAbout);
        menu->Append( helpMenu, wxT("&Help") );

        SetMenuBar(menu);

        statusBar = CreateStatusBar(2, wxST_SIZEGRIP, wxID_ANY);
        wxBoxSizer* frameSizer;
        frameSizer = new wxBoxSizer(wxVERTICAL);

        tabbedPanel = new wxNotebook(this, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, 0);
        serialTab = new wxPanel(tabbedPanel, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxTAB_TRAVERSAL);
        wxBoxSizer* serialTabSizer;
        serialTabSizer = new wxBoxSizer(wxVERTICAL);

        wxStaticBoxSizer* openCloseBox;
        openCloseBox = new wxStaticBoxSizer(
                new wxStaticBox(serialTab, OPEN_CLOSE_BOX,
                        wxT("Open/Close Port")), wxHORIZONTAL);

        openCloseBox->SetMinSize(wxSize(-1, 1));
        portNameTextLabel = new wxStaticText(serialTab, wxID_ANY,
                wxT("Port Name:"), wxDefaultPosition, wxDefaultSize, 0);
        portNameTextLabel->Wrap(-1);
        openCloseBox->Add(portNameTextLabel, 0, wxALL, 5);

        portNameText = new wxTextCtrl(serialTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        openCloseBox->Add(portNameText, 0, wxALL, 5);

        connectPortButton = new wxButton(serialTab, wxID_ANY, wxT("Connect"),
                wxDefaultPosition, wxDefaultSize, 0);
        openCloseBox->Add(connectPortButton, 0, wxALL, 5);

        connectedLight = new wxStaticBitmap(serialTab, wxID_ANY, wxNullBitmap,
                wxDefaultPosition, wxDefaultSize, 0);
        openCloseBox->Add(connectedLight, 0, wxALL, 5);

        serialTabSizer->Add(openCloseBox, 1, wxEXPAND, 5);

        serialTab->SetSizer(serialTabSizer);
        serialTab->Layout();
        serialTabSizer->Fit(serialTab);
        tabbedPanel->AddPage(serialTab, wxT("Serial Port"), true);
        registerTab = new wxPanel(tabbedPanel, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxTAB_TRAVERSAL);
        wxBoxSizer* registerTabSizer;
        registerTabSizer = new wxBoxSizer(wxHORIZONTAL);

        registerTree = new wxTreeCtrl(registerTab, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxTR_DEFAULT_STYLE|wxTR_HIDE_ROOT);
        registerTabSizer->Add(registerTree, 1, wxALL|wxEXPAND, 5);

        registerTab->SetSizer(registerTabSizer);
        registerTab->Layout();
        registerTabSizer->Fit(registerTab);
        tabbedPanel->AddPage(registerTab, wxT("Registers"), false);
        dateTimeTab = new wxPanel(tabbedPanel, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxTAB_TRAVERSAL);
        wxBoxSizer* dateTimeTabSizer;
        dateTimeTabSizer = new wxBoxSizer(wxHORIZONTAL);

        wxBoxSizer* dateTimeVerticalCollapseSizer;
        dateTimeVerticalCollapseSizer = new wxBoxSizer(wxVERTICAL);

        wxStaticBoxSizer* dateBoxSizer;
        dateBoxSizer = new wxStaticBoxSizer(new wxStaticBox( dateTimeTab,
                wxID_ANY, wxT("Date") ), wxHORIZONTAL);

        dateBoxSizer->Add( 0, 0, 1, wxEXPAND, 5 );

        yearTextLabel = new wxStaticText(dateTimeTab, wxID_ANY, wxT("Year: "),
                wxDefaultPosition, wxDefaultSize, 0);
        yearTextLabel->Wrap(-1);
        dateBoxSizer->Add(yearTextLabel, 0, wxALL, 5);

        yearText = new wxTextCtrl(dateTimeTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        dateBoxSizer->Add(yearText, 0, wxALL, 5);

        dateBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        monthTextLabel = new wxStaticText(dateTimeTab, wxID_ANY, wxT("Month: "),
                wxDefaultPosition, wxDefaultSize, 0 );
        monthTextLabel->Wrap( -1 );
        dateBoxSizer->Add( monthTextLabel, 0, wxALL, 5 );

        monthText = new wxTextCtrl(dateTimeTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        dateBoxSizer->Add(monthText, 0, wxALL, 5);

        dateBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        dayTextLabel = new wxStaticText(dateTimeTab, wxID_ANY, wxT("Day: "),
                wxDefaultPosition, wxDefaultSize, 0);
        dayTextLabel->Wrap(-1);
        dateBoxSizer->Add(dayTextLabel, 0, wxALL, 5);

        dayText = new wxTextCtrl(dateTimeTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        dateBoxSizer->Add( dayText, 0, wxALL, 5 );

        dateBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        dateTimeVerticalCollapseSizer->Add(dateBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* timeBoxSizer;
        timeBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox(dateTimeTab, wxID_ANY, wxT("Time")),
                wxHORIZONTAL);

        timeBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        hourTextLabel = new wxStaticText(dateTimeTab, wxID_ANY,
                wxT("Hour: "), wxDefaultPosition, wxDefaultSize, 0);
        hourTextLabel->Wrap(-1);
        timeBoxSizer->Add(hourTextLabel, 0, wxALL, 5);

        hourText = new wxTextCtrl(dateTimeTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        timeBoxSizer->Add(hourText, 0, wxALL, 5);

        timeBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        minuteTextLabel = new wxStaticText(dateTimeTab, wxID_ANY,
                wxT("Minutes: "), wxDefaultPosition, wxDefaultSize, 0);
        minuteTextLabel->Wrap(-1);
        timeBoxSizer->Add( minuteTextLabel, 0, wxALL, 5 );

        minuteText = new wxTextCtrl(dateTimeTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        timeBoxSizer->Add(minuteText, 0, wxALL, 5);

        timeBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        secondTextLabel = new wxStaticText(dateTimeTab, wxID_ANY,
                wxT("Seconds: "), wxDefaultPosition, wxDefaultSize, 0 );
        secondTextLabel->Wrap(-1);
        timeBoxSizer->Add(secondTextLabel, 0, wxALL, 5);

        secondText = new wxTextCtrl(dateTimeTab, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0);
        timeBoxSizer->Add(secondText, 0, wxALL, 5);

        timeBoxSizer->Add(0, 0, 1, wxEXPAND, 5);

        dateTimeVerticalCollapseSizer->Add(timeBoxSizer, 1, wxEXPAND, 5);

        wxBoxSizer* dateTimeButtonsSizer;
        dateTimeButtonsSizer = new wxBoxSizer(wxHORIZONTAL);

        dateTimeButtonsSizer->Add(0, 0, 1, wxEXPAND, 5);

        getDateTimeButton = new wxButton(dateTimeTab, wxID_ANY,
                wxT("Get Date-Time"), wxDefaultPosition, wxDefaultSize, 0);
        dateTimeButtonsSizer->Add(getDateTimeButton, 0, wxALL, 5);

        setDateTimeButton = new wxButton(dateTimeTab, wxID_ANY,
                wxT("Set Date-Time"), wxDefaultPosition, wxDefaultSize, 0);
        dateTimeButtonsSizer->Add(setDateTimeButton, 0, wxALL, 5);

        dateTimeButtonsSizer->Add(0, 0, 1, wxEXPAND, 5);

        dateTimeVerticalCollapseSizer->Add(
                dateTimeButtonsSizer, 1, wxEXPAND, 5);

        dateTimeTabSizer->Add(dateTimeVerticalCollapseSizer, 1, 0, 5);

        dateTimeTab->SetSizer(dateTimeTabSizer);
        dateTimeTab->Layout();
        dateTimeTabSizer->Fit(dateTimeTab);
        tabbedPanel->AddPage(dateTimeTab, wxT("Date-Time"), false);
        commandTab = new wxPanel(tabbedPanel, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxTAB_TRAVERSAL);
        wxBoxSizer* commandTabSizer;
        commandTabSizer = new wxBoxSizer(wxHORIZONTAL);

        wxBoxSizer* commandTabVerticalCollapseSizer;
        commandTabVerticalCollapseSizer = new wxBoxSizer(wxVERTICAL);

        wxStaticBoxSizer* generalCommandBoxSizer;
        generalCommandBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox(commandTab, wxID_ANY, wxT("General")),
                wxHORIZONTAL);

        nullButton = new wxButton(commandTab, wxID_ANY, wxT("Echo"),
                wxDefaultPosition, wxDefaultSize, 0);
        generalCommandBoxSizer->Add(nullButton, 0, wxALL, 5);

        factoryResetButton = new wxButton(commandTab, wxID_ANY,
                wxT("Factory Reset"), wxDefaultPosition, wxDefaultSize, 0);
        generalCommandBoxSizer->Add(factoryResetButton, 0, wxALL, 5);

        softwareResetButton = new wxButton(commandTab, wxID_ANY,
                wxT("Software Reset"), wxDefaultPosition, wxDefaultSize, 0);
        generalCommandBoxSizer->Add(softwareResetButton, 0, wxALL, 5);

        sleepButton = new wxButton(commandTab, wxID_ANY, wxT("Sleep"),
                wxDefaultPosition, wxDefaultSize, 0);
        generalCommandBoxSizer->Add(sleepButton, 0, wxALL, 5);

        sleepResetButton = new wxButton(commandTab, wxID_ANY,
                wxT("Sleep Reset"), wxDefaultPosition, wxDefaultSize, 0);
        generalCommandBoxSizer->Add(sleepResetButton, 0, wxALL, 5);

        commandTabVerticalCollapseSizer->Add(
                generalCommandBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* gyroscopeCommandBoxSizer;
        gyroscopeCommandBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox(commandTab, wxID_ANY, wxT("Gyroscope")),
                wxHORIZONTAL );

        gyroscopeSampleAxisButton = new wxButton(commandTab, wxID_ANY,
                wxT("Sample Axis"), wxDefaultPosition, wxDefaultSize, 0);
        gyroscopeCommandBoxSizer->Add(gyroscopeSampleAxisButton, 0, wxALL, 5);

        gyroscopeCalculateSensitivityButton = new wxButton(commandTab, wxID_ANY,
                wxT("Calculate Sensitivity"), wxDefaultPosition,
                wxDefaultSize, 0);
        gyroscopeCommandBoxSizer->Add(gyroscopeCalculateSensitivityButton, 0, wxALL, 5);

        gyroscopeBias1Button = new wxButton(commandTab, wxID_ANY,
                wxT("Sample Bias 1"), wxDefaultPosition, wxDefaultSize, 0);
        gyroscopeCommandBoxSizer->Add(gyroscopeBias1Button, 0, wxALL, 5);

        gyroscopeBias2Button = new wxButton(commandTab, wxID_ANY,
                wxT("Sample Bias 2"), wxDefaultPosition, wxDefaultSize, 0);
        gyroscopeCommandBoxSizer->Add(gyroscopeBias2Button, 0, wxALL, 5);

        gyroscopeCalculateBiasButton = new wxButton(commandTab, wxID_ANY,
                wxT("Calculate Bias"), wxDefaultPosition, wxDefaultSize, 0);
        gyroscopeCommandBoxSizer->Add(
                gyroscopeCalculateBiasButton, 0, wxALL, 5);

        commandTabVerticalCollapseSizer->Add(
                gyroscopeCommandBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* accelerometerCommandBoxSizer;
        accelerometerCommandBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox(commandTab, wxID_ANY, wxT("Acclerometer")),
                wxHORIZONTAL);

        accelerometerSampleAxisButton = new wxButton(commandTab, wxID_ANY,
                wxT("Sample Axis"), wxDefaultPosition, wxDefaultSize, 0);
        accelerometerCommandBoxSizer->Add(
                accelerometerSampleAxisButton, 0, wxALL, 5);

        accelerometerCalculateButton = new wxButton(commandTab, wxID_ANY,
                wxT("Calculate Sensitivity"), wxDefaultPosition,
                wxDefaultSize, 0);
        accelerometerCommandBoxSizer->Add(
                accelerometerCalculateButton, 0, wxALL, 5);

        commandTabVerticalCollapseSizer->Add(
                accelerometerCommandBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* magnetometerCommandBoxSizer;
        magnetometerCommandBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox( commandTab, wxID_ANY, wxT("Magnetometer")),
                wxHORIZONTAL);

        magnetometerMeasureButton = new wxButton(commandTab, wxID_ANY,
                wxT("Measure Sensitivity"), wxDefaultPosition,
                wxDefaultSize, 0);
        magnetometerCommandBoxSizer->Add(
                magnetometerMeasureButton, 0, wxALL, 5);

        commandTabVerticalCollapseSizer->Add(
                magnetometerCommandBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* algorithmCommandBoxSizer;
        algorithmCommandBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox( commandTab, wxID_ANY, wxT("Algorithm")),
                wxHORIZONTAL);

        algorithmInitialiseButton = new wxButton(commandTab, wxID_ANY,
                wxT("Initialise"), wxDefaultPosition, wxDefaultSize, 0);
        algorithmCommandBoxSizer->Add(algorithmInitialiseButton, 0, wxALL, 5);

        algorithmTareButton = new wxButton(commandTab, wxID_ANY, wxT("Tare"),
                wxDefaultPosition, wxDefaultSize, 0);
        algorithmCommandBoxSizer->Add(algorithmTareButton, 0, wxALL, 5);

        algorithmClearTareButton = new wxButton(commandTab, wxID_ANY,
                wxT("Clear Tare"), wxDefaultPosition, wxDefaultSize, 0);
        algorithmCommandBoxSizer->Add(algorithmClearTareButton, 0, wxALL, 5);

        algorithmInitialiseTareButton = new wxButton(commandTab, wxID_ANY,
                wxT("Init and Tare"), wxDefaultPosition, wxDefaultSize, 0);
        algorithmCommandBoxSizer->Add(
                algorithmInitialiseTareButton, 0, wxALL, 5);

        commandTabVerticalCollapseSizer->Add(
                algorithmCommandBoxSizer, 1, wxEXPAND, 5);

        commandTabSizer->Add(commandTabVerticalCollapseSizer, 1, 0, 5);

        commandTab->SetSizer(commandTabSizer);
        commandTab->Layout();
        commandTabSizer->Fit(commandTab);
        tabbedPanel->AddPage(commandTab, wxT("Commands"), false);
        sensorDataTab = new wxPanel(tabbedPanel, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, wxTAB_TRAVERSAL);
        wxBoxSizer* sensorDataTabSizer;
        sensorDataTabSizer = new wxBoxSizer(wxHORIZONTAL);

        wxBoxSizer* sensorDataTabVerticalCollapse;
        sensorDataTabVerticalCollapse = new wxBoxSizer(wxVERTICAL);

        wxStaticBoxSizer* batteryThermometerDataBoxSizer;
        batteryThermometerDataBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox( sensorDataTab, wxID_ANY,
                        wxT("Battery and Thermometer Data") ),
                wxHORIZONTAL);

        showBatteryGraphButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show Battery Graph"), wxDefaultPosition, wxDefaultSize, 0);
        batteryThermometerDataBoxSizer->Add(
                showBatteryGraphButton, 0, wxALL, 5);

        showThermometerGraphButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show Thermometer Graph"), wxDefaultPosition,
                wxDefaultSize, 0);
        batteryThermometerDataBoxSizer->Add(
                showThermometerGraphButton, 0, wxALL, 5);

        sensorDataTabVerticalCollapse->Add(
                batteryThermometerDataBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* inertiaMagneticDataBoxSizer;
        inertiaMagneticDataBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox(sensorDataTab, wxID_ANY,
                        wxT("Inertia and Magnetic Sensor Data")),
                wxHORIZONTAL );

        showGyroscopeGraphButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show Gyroscope Graph"), wxDefaultPosition,
                wxDefaultSize, 0);
        inertiaMagneticDataBoxSizer->Add(showGyroscopeGraphButton, 0, wxALL, 5);

        showAccelerometerGraphButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show Accelerometer Graph"), wxDefaultPosition,
                wxDefaultSize, 0);
        inertiaMagneticDataBoxSizer->Add(
                showAccelerometerGraphButton, 0, wxALL, 5);

        showMagnetometerGraphButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show Magnetometer Graph"), wxDefaultPosition,
                wxDefaultSize, 0);
        inertiaMagneticDataBoxSizer->Add(
                showMagnetometerGraphButton, 0, wxALL, 5);

        sensorDataTabVerticalCollapse->Add(
                inertiaMagneticDataBoxSizer, 1, wxEXPAND, 5);

        wxStaticBoxSizer* orientationDataBoxSizer;
        orientationDataBoxSizer = new wxStaticBoxSizer(
                new wxStaticBox( sensorDataTab, wxID_ANY,
                        wxT("Orientation Data")),
                wxHORIZONTAL);

        show3DCuboidButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show 3D Cuboid"), wxDefaultPosition, wxDefaultSize, 0);
        orientationDataBoxSizer->Add(show3DCuboidButton, 0, wxALL, 5);

        showEulerAngleGraphButton = new wxButton(sensorDataTab, wxID_ANY,
                wxT("Show Euler Angle Graph"), wxDefaultPosition,
                wxDefaultSize, 0);
        orientationDataBoxSizer->Add(showEulerAngleGraphButton, 0, wxALL, 5);

        sensorDataTabVerticalCollapse->Add(
                orientationDataBoxSizer, 1, wxEXPAND, 5);

        sensorDataTabSizer->Add(sensorDataTabVerticalCollapse, 1, 0, 5);

        sensorDataTab->SetSizer(sensorDataTabSizer);
        sensorDataTab->Layout();
        sensorDataTabSizer->Fit(sensorDataTab);
        tabbedPanel->AddPage(sensorDataTab, wxT("Sensor Data"), false);

        frameSizer->Add(tabbedPanel, 1, wxEXPAND | wxALL, 5);

        SetSizer(frameSizer);
        Layout();

        // Bind Events
        Bind(wxEVT_CLOSE_WINDOW, &GUIFrame::OnClose, this);
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::OnQuit, this,
                menuFileQuit->GetId());
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::OnAbout, this,
                menuHelpAbout->GetId());
        tabbedPanel->Bind(wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED,
                &GUIFrame::tabbedChanged, this);
        portNameText->Bind(wxEVT_COMMAND_TEXT_UPDATED,
                &GUIFrame::portNameChanged, this);
        connectPortButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::connectPort, this);
        getDateTimeButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::getDateTime, this);
        setDateTimeButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::setDateTime, this);
        nullButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::nullCommand, this);
        factoryResetButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::factoryResetCommand, this);
        softwareResetButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::softwareResetCommand, this);
        sleepButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::sleepCommand, this);
        sleepResetButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::sleepResetCommand, this);
        gyroscopeSampleAxisButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeSampleAxisCommand, this);
        gyroscopeCalculateSensitivityButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeCalculateSensitivityCommand, this);
        gyroscopeBias1Button->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeBias1Command, this);
        gyroscopeBias2Button->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeBias2Command, this);
        gyroscopeCalculateBiasButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeCalculateBiasCommand, this);
        accelerometerSampleAxisButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::accelerometerSampleAxisCommand, this);
        accelerometerCalculateButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::accelerometerCalculateCommand, this);
        magnetometerMeasureButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::magnetometerMeasureCommand, this);
        algorithmInitialiseButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmInitialiseCommand, this);
        algorithmTareButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmTareCommand, this);
        algorithmClearTareButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmClearTareCommand, this);
        algorithmInitialiseTareButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmInitialiseTareCommand, this);
        showBatteryGraphButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showBatteryGraph, this);
        showThermometerGraphButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showThermometerGraph, this);
        showGyroscopeGraphButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showGyroscopeGraph, this);
        showAccelerometerGraphButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showAccelerometerGraph, this);
        showMagnetometerGraphButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showMagnetometerGraph, this);
        show3DCuboidButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::show3DCuboid, this);
        showEulerAngleGraphButton->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showEulerAngleGraph, this);

        connectedLight->SetBitmap(blueLight);
        buildRegisterTree();
        api = new API();
        guiListener = new GUIListener(this);
        api->addListener(guiListener);

        Bind(wxEVT_TREE_ITEM_EXPANDED, &GUIFrame::treeItemExpanded, this);
        Bind(wxEVT_TREE_ITEM_RIGHT_CLICK, &GUIFrame::treeItemRightClick, this);
    }

    GUIFrame::~GUIFrame()
    {
        delete api;
        delete guiListener;

        // Unbind Events
        Unbind(wxEVT_CLOSE_WINDOW, &GUIFrame::OnClose, this);
        Unbind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::OnQuit, this);
        Unbind(wxEVT_COMMAND_MENU_SELECTED, &GUIFrame::OnAbout, this);
        tabbedPanel->Unbind(wxEVT_COMMAND_NOTEBOOK_PAGE_CHANGED,
                &GUIFrame::tabbedChanged, this);
        portNameText->Unbind(wxEVT_COMMAND_TEXT_UPDATED,
                &GUIFrame::portNameChanged, this);
        connectPortButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::connectPort, this);
        getDateTimeButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::getDateTime, this);
        setDateTimeButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::setDateTime, this);
        nullButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::nullCommand, this);
        factoryResetButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::factoryResetCommand, this);
        softwareResetButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::softwareResetCommand, this);
        sleepButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::sleepCommand, this);
        sleepResetButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::sleepResetCommand, this);
        gyroscopeSampleAxisButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeSampleAxisCommand, this);
        gyroscopeCalculateSensitivityButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeCalculateSensitivityCommand, this);
        gyroscopeBias1Button->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeBias1Command, this);
        gyroscopeBias2Button->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeBias2Command, this);
        gyroscopeCalculateBiasButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::gyroscopeCalculateBiasCommand, this);
        accelerometerSampleAxisButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::accelerometerSampleAxisCommand, this);
        accelerometerCalculateButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::accelerometerCalculateCommand, this);
        magnetometerMeasureButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::magnetometerMeasureCommand, this);
        algorithmInitialiseButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmInitialiseCommand, this);
        algorithmTareButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmTareCommand, this);
        algorithmClearTareButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmClearTareCommand, this);
        algorithmInitialiseTareButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::algorithmInitialiseTareCommand, this);
        showBatteryGraphButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showBatteryGraph, this);
        showThermometerGraphButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showThermometerGraph, this);
        showGyroscopeGraphButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showGyroscopeGraph, this);
        showAccelerometerGraphButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showAccelerometerGraph, this);
        showMagnetometerGraphButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showMagnetometerGraph, this);
        show3DCuboidButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::show3DCuboid, this);
        showEulerAngleGraphButton->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &GUIFrame::showEulerAngleGraph, this);
        Unbind(wxEVT_TREE_ITEM_EXPANDED, &GUIFrame::treeItemExpanded, this);
        Unbind(wxEVT_TREE_ITEM_RIGHT_CLICK, &GUIFrame::treeItemRightClick, this);
    }

    void GUIFrame::updateStatusBar(const wxString& status)
    {
        //statusBar->SetStatusText(status);
        statusBar->GetEventHandler()->CallAfter(
            &wxStatusBar::SetStatusText, status, 0);
    }

    void GUIFrame::updateRegisterData(const RegisterAddress& address,
            const float value)
    {
        wxTreeItemId registerItem =
            getRegisterTreeItem(registerTree->GetRootItem(), address);
        if (registerItem.IsOk())
        {
            RegisterTreeItemData* registerData =
                (RegisterTreeItemData*)
                registerTree->GetItemData(registerItem);
            registerData->setValue(value);
            // This will cause an occasional segmentation fault, because
            // we're updating the GUI from outside the GUI thread, but
            // the event handler, commented out below, is not calling
            // the delayed call. Further investigation required.
            registerTree->SetItemText(registerItem, registerData->getText());
            /*
            registerTree->GetEventHandler()->CallAfter(
                &wxTreeCtrl::SetItemText,
                registerItem,
                registerData->getText());
            */
            wxString statusMessage = registerData->getName();
            statusMessage << " set to " << registerData->getValue();
            updateStatusBar(statusMessage);
        }
        else
        {
            std::cout << "Register " << (int)address << " not found!" << std::endl;
        }
    }

    void GUIFrame::updateDateTime(
        const wxString& year,
        const wxString& month,
        const wxString& day,
        const wxString& hour,
        const wxString& minute,
        const wxString& second)
    {
        //yearText->SetValue(year);
        yearText->GetEventHandler()->CallAfter(
            &wxTextCtrl::SetValue, year);
        //monthText->SetValue(month);
        monthText->GetEventHandler()->CallAfter(
            &wxTextCtrl::SetValue, month);
        //dayText->SetValue(day);
        dayText->GetEventHandler()->CallAfter(
            &wxTextCtrl::SetValue, day);
        //hourText->SetValue(hour);
        hourText->GetEventHandler()->CallAfter(
            &wxTextCtrl::SetValue, hour);
        //minuteText->SetValue(minute);
        minuteText->GetEventHandler()->CallAfter(
            &wxTextCtrl::SetValue, minute);
        //secondText->SetValue(second);
        secondText->GetEventHandler()->CallAfter(
            &wxTextCtrl::SetValue, second);

        updateStatusBar(wxString("Current date and time received."));
    }

    void GUIFrame::OnClose(wxCloseEvent &event)
    {
        Destroy();
    }

    void GUIFrame::OnQuit(wxCommandEvent &event)
    {
        Destroy();
    }

    void GUIFrame::OnAbout(wxCommandEvent &event)
    {
        wxAboutDialogInfo info;
        info.SetName(wxT("Cross Platform xIMU GUI"));
        info.SetVersion(wxT("v 0.1"));
        info.SetDescription(wxT("This a cross-platform C++ version of the x-IO IMU GUI."));
        info.SetCopyright(wxT("(c) 2014 Marcel Admiraal"));
        wxAboutBox(info);
    }

    void GUIFrame::tabbedChanged(wxNotebookEvent &event)
    {
        statusBar->SetStatusText(wxT(""));
    }

    void GUIFrame::portNameChanged(wxCommandEvent &event)
    {
        connectedLight->SetBitmap(blueLight);
    }

    void GUIFrame::connectPort(wxCommandEvent &event)
    {
        wxString port = portNameText->GetLineText(0);
        bool success = api->setPort(ma::Str(port.mb_str()));
        if (success)
        {
            connectedLight->SetBitmap(greenLight);
            wxString status = wxT("Connected to ") + port;
            statusBar->SetStatusText(status);
        }
        else
        {
            connectedLight->SetBitmap(redLight);
            wxString status = wxT("Faild to connect to ") + port;
            statusBar->SetStatusText(status);
        }
    }

    void GUIFrame::getDateTime(wxCommandEvent &event)
    {
        statusBar->SetStatusText(wxT("Getting date and time..."));
        api->sendReadDateTime();
    }

    void GUIFrame::setDateTime(wxCommandEvent &event)
    {
        bool valid = true;
        long year, month, day, hour, minute, second;
        if(!yearText->GetValue().ToLong(&year)) valid = false;
        if(!monthText->GetValue().ToLong(&month)) valid = false;
        if(!dayText->GetValue().ToLong(&day)) valid = false;
        if(!hourText->GetValue().ToLong(&hour)) valid = false;
        if(!minuteText->GetValue().ToLong(&minute)) valid = false;
        if(!secondText->GetValue().ToLong(&second)) valid = false;

        if (valid)
        {
            api->sendWriteDateTime((int)year, (int)month, (int) day,
                (int)hour, (int)minute, (int)second);
            statusBar->SetStatusText("Setting date and time...");
        }
        else
        {
            statusBar->SetStatusText("Invalid date and time values!");
        }
    }

    void GUIFrame::nullCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::NullCommand);
    }

    void GUIFrame::factoryResetCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::FactoryReset);
    }

    void GUIFrame::softwareResetCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::Reset);
    }

    void GUIFrame::sleepCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::Sleep);
    }

    void GUIFrame::sleepResetCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::ResetSleepTimer);
    }

    void GUIFrame::gyroscopeSampleAxisCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::SampleGyroscopeAxisAt200dps);
    }

    void GUIFrame::gyroscopeCalculateSensitivityCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::CalculateGyroscopeSensitivity);
    }

    void GUIFrame::gyroscopeBias1Command(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::SampleGyroscopeBiasTemp1);
    }

    void GUIFrame::gyroscopeBias2Command(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::SampleGyroscopeBiasTemp2);
    }

    void GUIFrame::gyroscopeCalculateBiasCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::CalculateGyroscopeBiasParameters);
    }

    void GUIFrame::accelerometerSampleAxisCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::SampleAccelerometerAxisAt1g);
    }

    void GUIFrame::accelerometerCalculateCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::CalculateAccelerometerBiasAndSensitivity);
    }

    void GUIFrame::magnetometerMeasureCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::MeasureMagnetometerBiasAndSensitivity);
    }

    void GUIFrame::algorithmInitialiseCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::AlgorithmInitialise);
    }

    void GUIFrame::algorithmTareCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::AlgorithmTare);
    }

    void GUIFrame::algorithmClearTareCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::AlgorithmClearTare);
    }

    void GUIFrame::algorithmInitialiseTareCommand(wxCommandEvent &event)
    {
        api->sendCommand(CommandCode::AlgorithmInitialiseThenTare);
    }

    void GUIFrame::showBatteryGraph(wxCommandEvent &event)
    {
        GraphFrame* graphFrame = new VoltageGraphFrame(this, api);
        graphFrame->Show();
    }

    void GUIFrame::showThermometerGraph(wxCommandEvent &event)
    {
        GraphFrame* graphFrame = new TemperatureGraphFrame(this, api);
        graphFrame->Show();
    }

    void GUIFrame::showGyroscopeGraph(wxCommandEvent &event)
    {
        GraphFrame* graphFrame = new GyroscopeGraphFrame(this, api);
        graphFrame->Show();
    }

    void GUIFrame::showAccelerometerGraph(wxCommandEvent &event)
    {
        GraphFrame* graphFrame = new AccelerometerGraphFrame(this, api);
        graphFrame->Show();
    }

    void GUIFrame::showMagnetometerGraph(wxCommandEvent &event)
    {
        GraphFrame* graphFrame = new MagnetometerGraphFrame(this, api);
        graphFrame->Show();
    }

    void GUIFrame::show3DCuboid(wxCommandEvent &event)
    {
        GraphFrame* graphFrame = new CuboidFrame(this, api);
        graphFrame->Show();
    }

    void GUIFrame::showEulerAngleGraph(wxCommandEvent &event)
    {
    }

    void GUIFrame::buildRegisterTree()
    {
        RegisterTreeItemData* data;
        wxArrayString mapValue;

        // Create root item.
        wxTreeItemId root = registerTree->AddRoot(
            wxT("Registers"));

        // Create first level items.
        wxTreeItemId general = registerTree->AppendItem(
            root, wxT("General"));
        wxTreeItemId calibration = registerTree->AppendItem(
            root, wxT("Sensor Calibration Parameters"));
        wxTreeItemId algorithm = registerTree->AppendItem(
            root, wxT("Algorithm Parameters"));
        wxTreeItemId output = registerTree->AppendItem(
            root, wxT("Data Output Settings"));
        wxTreeItemId sdCard = registerTree->AppendItem(
            root, wxT("SD Card"));
        wxTreeItemId power = registerTree->AppendItem(
            root, wxT("Power Management"));
        wxTreeItemId auxillary = registerTree->AppendItem(
            root, wxT("Auxillary Port"));

        // Create General items.
        wxTreeItemId firmware = registerTree->AppendItem(
            general, wxT("Firmware Version"));
        // Firmware Version Major Number
        data = new RegisterTreeItemData(
                wxT("Major Number"),
                RegisterAddress::FirmwareVersionMajorNum,
                false);
        registerTree->AppendItem(
            firmware, data->getText(), -1, -1, data);
        // Firmware Version Minor Number
        data = new RegisterTreeItemData(
                wxT("Minor Number"),
                RegisterAddress::FirmwareVersionMinorNum,
                false);
        registerTree->AppendItem(firmware, data->getText(), -1, -1, data);
        // Device ID
        data = new RegisterTreeItemData(
                wxT("Device ID"),
                RegisterAddress::DeviceID,
                false, (QValue)0, wxEmptyString, true);
        registerTree->AppendItem(
            general, data->getText(), -1, -1, data);
        // Button Mode
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("Reset Command"));
            mapValue.Add(wxT("Sleep/Wakeup"));
            mapValue.Add(wxT("Algorithm Initialise Command"));
            mapValue.Add(wxT("Algorithm Tare Command"));
            mapValue.Add(wxT("Algorithm Initialise then Tare Command"));
        data = new RegisterTreeItemData(
                wxT("Button Mode"),
                RegisterAddress::ButtonMode,
                mapValue);
        registerTree->AppendItem(
            general, data->getText(), -1, -1, data);

        // Create Sensor Calibration Parameters Items.

        // Create Battery Items.
        wxTreeItemId battery = registerTree->AppendItem(
            calibration, wxT("Battery Voltmeter"));
        // Battery Voltmeter Sensitivity
        data = new RegisterTreeItemData(
                wxT("Sensitivity"),
                RegisterAddress::BatterySensitivity,
                true, QValue::BatterySensitivity);
        registerTree->AppendItem(
            battery, data->getText(), -1, -1, data);
        // Battery Voltmeter Bias
        data = new RegisterTreeItemData(
                wxT("Bias"),
                RegisterAddress::BatteryBias,
                true, QValue::BatteryBias);
        registerTree->AppendItem(
            battery, data->getText(), -1, -1, data);
        // Thermometer
        wxTreeItemId thermometer = registerTree->AppendItem(
            calibration, wxT("Thermometer"));
        // Thermometer Sensitivity
        data = new RegisterTreeItemData(
                wxT("Sensitivity"),
                RegisterAddress::ThermometerSensitivity,
                true, QValue::ThermometerSensitivity);
        registerTree->AppendItem(
            thermometer, data->getText(), -1, -1, data);
        // Thermometer Bias
        data = new RegisterTreeItemData(
                wxT("Bias"),
                RegisterAddress::ThermometerBias,
                true, QValue::ThermometerBias);
        registerTree->AppendItem(
            thermometer, data->getText(), -1, -1, data);

        // Create Gyroscope Items.
        wxTreeItemId gyroscope = registerTree->AppendItem(
            calibration, wxT("Gyroscope"));
        // Gyroscope Full-Scale
        mapValue.Empty();
            mapValue.Add(wxT("±250"));
            mapValue.Add(wxT("±500"));
            mapValue.Add(wxT("±1000"));
            mapValue.Add(wxT("±2000"));
        data = new RegisterTreeItemData(
                wxT("Full-Scale"),
                RegisterAddress::GyroscopeFullScale,
                mapValue, wxT("°/s"));
        registerTree->AppendItem(
            gyroscope, data->getText(), -1, -1, data);
        // Gyryscope Sensitivity
        wxTreeItemId gyroscopeSensitivity = registerTree->AppendItem(
            gyroscope, wxT("Sensitivity"));
        // Gyroscope X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeSensitivityX,
                true, QValue::GyroscopeSensitivity);
        registerTree->AppendItem(
            gyroscopeSensitivity, data->getText(), -1, -1, data);
        // Gyroscope Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeSensitivityY,
                true, QValue::GyroscopeSensitivity);
        registerTree->AppendItem(
            gyroscopeSensitivity, data->getText(), -1, -1, data);
        // Gyroscope Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeSensitivityZ,
                true, QValue::GyroscopeSensitivity);
        registerTree->AppendItem(
            gyroscopeSensitivity, data->getText(), -1, -1, data);
        // Gyryscope Sampled at +200°/s
        wxTreeItemId gyroscopeSampledp200 = registerTree->AppendItem(
            gyroscope, wxT("Sampled at +200°/s"));
        // Gyroscope X-Axis Sampled at +200°/s
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeSampledPlus200dpsX,
                true, QValue::GyroscopeSampled200dps);
        registerTree->AppendItem(
            gyroscopeSampledp200, data->getText(), -1, -1, data);
        // Gyroscope Y-Axis Sampled at +200°/s
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeSampledPlus200dpsY,
                true, QValue::GyroscopeSampled200dps);
        registerTree->AppendItem(
            gyroscopeSampledp200, data->getText(), -1, -1, data);
        // Gyroscope Z-Axis Sampled at +200°/s
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeSampledPlus200dpsZ,
                true, QValue::GyroscopeSampled200dps);
        registerTree->AppendItem(
            gyroscopeSampledp200, data->getText(), -1, -1, data);
        // Gyryscope Sampled at -200°/s
        wxTreeItemId gyroscopeSampledm200 = registerTree->AppendItem(
            gyroscope, wxT("Sampled at -200°/s"));
        // Gyroscope X-Axis Sampled at -200°/s
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeSampledMinus200dpsX,
                true, QValue::GyroscopeSampled200dps);
        registerTree->AppendItem(
            gyroscopeSampledm200, data->getText(), -1, -1, data);
        // Gyroscope Y-Axis Sampled at -200°/s
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeSampledMinus200dpsY,
                true, QValue::GyroscopeSampled200dps);
        registerTree->AppendItem(
            gyroscopeSampledm200, data->getText(), -1, -1, data);
        // Gyroscope Z-Axis Sampled at -200°/s
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeSampledMinus200dpsZ,
                true, QValue::GyroscopeSampled200dps);
        registerTree->AppendItem(
            gyroscopeSampledm200, data->getText(), -1, -1, data);
        // Gyryscope Bias at 25°C
        wxTreeItemId gyroscopeBias = registerTree->AppendItem(
            gyroscope, wxT("Bias at 25°C"));
        // Gyroscope X-Axis Bias at 25°C
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeBiasAt25degCX,
                true, QValue::GyroscopeBiasAt25degC);
        registerTree->AppendItem(
            gyroscopeBias, data->getText(), -1, -1, data);
        // Gyroscope Y-Axis Bias at 25°C
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeBiasAt25degCY,
                true, QValue::GyroscopeBiasAt25degC);
        registerTree->AppendItem(
            gyroscopeBias, data->getText(), -1, -1, data);
        // Gyroscope Z-Axis Bias at 25°C
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeBiasAt25degCZ,
                true, QValue::GyroscopeBiasAt25degC);
        registerTree->AppendItem(
            gyroscopeBias, data->getText(), -1, -1, data);
        // Gyryscope Bias Temperature Sensitivity
        wxTreeItemId gyroscopeBiasSensitivity = registerTree->AppendItem(
            gyroscope, wxT("Bias Temperature Sensitivity"));
        // Gyroscope X-Axis Bias Temperature Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeBiasTempSensitivityX,
                true, QValue::GyroscopeBiasTempSensitivity);
        registerTree->AppendItem(
            gyroscopeBiasSensitivity, data->getText(), -1, -1, data);
        // Gyroscope Y-Axis Bias Temperature Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeBiasTempSensitivityY,
                true, QValue::GyroscopeBiasTempSensitivity);
        registerTree->AppendItem(
            gyroscopeBiasSensitivity, data->getText(), -1, -1, data);
        // Gyroscope Z-Axis Bias Temperature Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeBiasTempSensitivityZ,
                true, QValue::GyroscopeBiasTempSensitivity);
        registerTree->AppendItem(
            gyroscopeBiasSensitivity, data->getText(), -1, -1, data);
        // Gyryscope Sample 1
        wxTreeItemId gyroscopeSample1 = registerTree->AppendItem(
            gyroscope, wxT("Sample 1"));
        // Gyroscope Sample 1 Temperature
        data = new RegisterTreeItemData(
                wxT("Temperature"),
                RegisterAddress::GyroscopeSample1Temp,
                true, QValue::CalibratedThermometer);
        registerTree->AppendItem(
            gyroscopeSample1, data->getText(), -1, -1, data);
        // Gyroscope Sample 1 X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeSample1BiasX,
                true, QValue::GyroscopeSampledBias);
        registerTree->AppendItem(
            gyroscopeSample1, data->getText(), -1, -1, data);
        // Gyroscope Sample 1 Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeSample1BiasY,
                true, QValue::GyroscopeSampledBias);
        registerTree->AppendItem(
            gyroscopeSample1, data->getText(), -1, -1, data);
        // Gyroscope Sample 1 Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeSample1BiasZ,
                true, QValue::GyroscopeSampledBias);
        registerTree->AppendItem(
            gyroscopeSample1, data->getText(), -1, -1, data);
        // Gyryscope Sample 2
        wxTreeItemId gyroscopeSample2 = registerTree->AppendItem(
            gyroscope, wxT("Sample 2"));
        // Gyroscope Sample 2 Temperature
        data = new RegisterTreeItemData(
                wxT("Temperature"),
                RegisterAddress::GyroscopeSample2Temp,
                true, QValue::CalibratedThermometer);
        registerTree->AppendItem(
            gyroscopeSample2, data->getText(), -1, -1, data);
        // Gyroscope Sample 2 X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::GyroscopeSample2BiasX,
                true, QValue::GyroscopeSampledBias);
        registerTree->AppendItem(
            gyroscopeSample2, data->getText(), -1, -1, data);
        // Gyroscope Sample 2 Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::GyroscopeSample2BiasY,
                true, QValue::GyroscopeSampledBias);
        registerTree->AppendItem(
            gyroscopeSample2, data->getText(), -1, -1, data);
        // Gyroscope Sample 2 Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::GyroscopeSample2BiasZ,
                true, QValue::GyroscopeSampledBias);
        registerTree->AppendItem(
            gyroscopeSample2, data->getText(), -1, -1, data);

        // Create Accelerometer Items.
        wxTreeItemId accelerometer = registerTree->AppendItem(
            calibration, wxT("Accelerometer"));
        // Accelerometer Full-Scale
        mapValue.Empty();
            mapValue.Add(wxT("±2"));
            mapValue.Add(wxT("±4"));
            mapValue.Add(wxT("±8"));
        data = new RegisterTreeItemData(
                wxT("Full-Scale"),
                RegisterAddress::AccelerometerFullScale,
                mapValue, wxT("g"));
        registerTree->AppendItem(
            accelerometer, data->getText(), -1, -1, data);
        // Accelerometer Sensitivity
        wxTreeItemId accelerometerSensitivity = registerTree->AppendItem(
            accelerometer, wxT("Sensitivity"));
        // Accelerometer X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::AccelerometerSensitivityX,
                true, QValue::AccelerometerSensitivity);
        registerTree->AppendItem(
            accelerometerSensitivity, data->getText(), -1, -1, data);
        // Accelerometer Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::AccelerometerSensitivityY,
                true, QValue::AccelerometerSensitivity);
        registerTree->AppendItem(
            accelerometerSensitivity, data->getText(), -1, -1, data);
        // Accelerometer Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::AccelerometerSensitivityZ,
                true, QValue::AccelerometerSensitivity);
        registerTree->AppendItem(
            accelerometerSensitivity, data->getText(), -1, -1, data);
        // Accelerometer Bias
        wxTreeItemId accelerometerBias = registerTree->AppendItem(
            accelerometer, wxT("Bias"));
        // Accelerometer X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::AccelerometerBiasX,
                true, QValue::AccelerometerBias);
        registerTree->AppendItem(
            accelerometerBias, data->getText(), -1, -1, data);
        // Accelerometer Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::AccelerometerBiasY,
                true, QValue::AccelerometerBias);
        registerTree->AppendItem(
            accelerometerBias, data->getText(), -1, -1, data);
        // Accelerometer Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::AccelerometerBiasZ,
                true, QValue::AccelerometerBias);
        registerTree->AppendItem(
            accelerometerBias, data->getText(), -1, -1, data);
        // Accelerometer Sampled at +1g
        wxTreeItemId accelerometerSampledp1g = registerTree->AppendItem(
            accelerometer, wxT("Sampled at +1g"));
        // Accelerometer X-Axis Sampled at +1g
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::AccelerometerSampledPlus1gX,
                true, QValue::AccelerometerSampled1g);
        registerTree->AppendItem(
            accelerometerSampledp1g, data->getText(), -1, -1, data);
        // Accelerometer Y-Axis Sampled at +1g
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::AccelerometerSampledPlus1gY,
                true, QValue::AccelerometerSampled1g);
        registerTree->AppendItem(
            accelerometerSampledp1g, data->getText(), -1, -1, data);
        // Accelerometer Z-Axis Sampled at +1g
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::AccelerometerSampledPlus1gZ,
                true, QValue::AccelerometerSampled1g);
        registerTree->AppendItem(
            accelerometerSampledp1g, data->getText(), -1, -1, data);
        // Accelerometer Sampled at -1g
        wxTreeItemId accelerometerSampledm1g = registerTree->AppendItem(
            accelerometer, wxT("Sampled at +1g"));
        // Accelerometer X-Axis Sampled at -1g
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::AccelerometerSampledMinus1gX,
                true, QValue::AccelerometerSampled1g);
        registerTree->AppendItem(
            accelerometerSampledm1g, data->getText(), -1, -1, data);
        // Accelerometer Y-Axis Sampled at -1g
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::AccelerometerSampledMinus1gY,
                true, QValue::AccelerometerSampled1g);
        registerTree->AppendItem(
            accelerometerSampledm1g, data->getText(), -1, -1, data);
        // Accelerometer Z-Axis Sampled at -1g
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::AccelerometerSampledMinus1gZ,
                true, QValue::AccelerometerSampled1g);
        registerTree->AppendItem(
            accelerometerSampledm1g, data->getText(), -1, -1, data);

        // Create Magnetometer Items.
        wxTreeItemId magnetometer = registerTree->AppendItem(
            calibration, wxT("Magnetometer"));
        // Magnetometer Full-Scale
        mapValue.Empty();
            mapValue.Add(wxT("±1.3"));
            mapValue.Add(wxT("±1.9"));
            mapValue.Add(wxT("±2.5"));
            mapValue.Add(wxT("±4.0"));
            mapValue.Add(wxT("±4.7"));
            mapValue.Add(wxT("±5.6"));
            mapValue.Add(wxT("±8.1"));
        data = new RegisterTreeItemData(
                wxT("Full-Scale"),
                RegisterAddress::MagnetometerFullScale,
                mapValue, wxT("G"));
        registerTree->AppendItem(
            magnetometer, data->getText(), -1, -1, data);
        // Magnetometer Sensitivity
        wxTreeItemId magnetometerSensitivity = registerTree->AppendItem(
            magnetometer, wxT("Sensitivity"));
        // Magnetometer X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::MagnetometerSensitivityX,
                true, QValue::MagnetometerSensitivity);
        registerTree->AppendItem(
            magnetometerSensitivity, data->getText(), -1, -1, data);
        // Magnetometer Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::MagnetometerSensitivityY,
                true, QValue::MagnetometerSensitivity);
        registerTree->AppendItem(
            magnetometerSensitivity, data->getText(), -1, -1, data);
        // Magnetometer Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::MagnetometerSensitivityZ,
                true, QValue::MagnetometerSensitivity);
        registerTree->AppendItem(
            magnetometerSensitivity, data->getText(), -1, -1, data);
        // Magnetometer Bias
        wxTreeItemId magnetometerBias = registerTree->AppendItem(
            magnetometer, wxT("Bias"));
        // Magnetometer X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::MagnetometerBiasX,
                true, QValue::MagnetometerBias);
        registerTree->AppendItem(
            magnetometerBias, data->getText(), -1, -1, data);
        // Magnetometer Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::MagnetometerBiasY,
                true, QValue::MagnetometerBias);
        registerTree->AppendItem(
            magnetometerBias, data->getText(), -1, -1, data);
        // Magnetometer Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::MagnetometerBiasZ,
                true, QValue::MagnetometerBias);
        registerTree->AppendItem(
            magnetometerBias, data->getText(), -1, -1, data);
        // Magnetometer Hard Iron Bias
        wxTreeItemId magnetometerHIBias = registerTree->AppendItem(
            magnetometer, wxT("Hard Iron Bias"));
        // Magnetometer X-Axis Hard Iron Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::MagnetometerHardIronBiasX,
                true, QValue::MagnetometerHardIronBias);
        registerTree->AppendItem(
            magnetometerHIBias, data->getText(), -1, -1, data);
        // Magnetometer Y-Axis Hard Iron Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::MagnetometerHardIronBiasY,
                true, QValue::MagnetometerHardIronBias);
        registerTree->AppendItem(
            magnetometerHIBias, data->getText(), -1, -1, data);
        // Magnetometer Z-Axis Hard Iron Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::MagnetometerHardIronBiasZ,
                true, QValue::MagnetometerHardIronBias);
        registerTree->AppendItem(
            magnetometerHIBias, data->getText(), -1, -1, data);

        // Create Algorithm items.
        // Algorithm Mode
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("IMU"));
            mapValue.Add(wxT("AHRS"));
        data = new RegisterTreeItemData(
                wxT("Mode"),
                RegisterAddress::AlgorithmMode,
                mapValue);
        registerTree->AppendItem(
            algorithm, data->getText(), -1, -1, data);
        // Algorithm Gains
        wxTreeItemId algorithmGains = registerTree->AppendItem(
            algorithm, wxT("Gains"));
        // Algorithm Kp Gain
        data = new RegisterTreeItemData(
                wxT("Kp"),
                RegisterAddress::AlgorithmKp,
                true, QValue::AlgorithmKp);
        registerTree->AppendItem(
            algorithmGains, data->getText(), -1, -1, data);
        // Algorithm Ki Gain
        data = new RegisterTreeItemData(
                wxT("Ki"),
                RegisterAddress::AlgorithmKi,
                true, QValue::AlgorithmKi);
        registerTree->AppendItem(
            algorithmGains, data->getText(), -1, -1, data);
        // Algorithm Initial Kp Gain
        data = new RegisterTreeItemData(
                wxT("Initial Kp"),
                RegisterAddress::AlgorithmInitKp,
                true, QValue::AlgorithmInitKp);
        registerTree->AppendItem(
            algorithmGains, data->getText(), -1, -1, data);
        // Algorithm Initialisation Period
        data = new RegisterTreeItemData(
                wxT("Initialisation Period"),
                RegisterAddress::AlgorithmInitPeriod,
                true, QValue::AlgorithmInitPeriod);
        registerTree->AppendItem(
            algorithmGains, data->getText(), -1, -1, data);
        // Algorithm Valid Magnetic Field
        wxTreeItemId algorithmValidField = registerTree->AppendItem(
            algorithm, wxT("Valid Magnetic Field Magnitude"));
        // Algorithm Minimum Valid Magnetic Field Magnitude
        data = new RegisterTreeItemData(
                wxT("Minimum"),
                RegisterAddress::AlgorithmMinValidMag,
                true, QValue::CalibratedMagnetometer);
        registerTree->AppendItem(
            algorithmValidField, data->getText(), -1, -1, data);
        // Algorithm Maximum Valid Magnetic Field Magnitude
        data = new RegisterTreeItemData(
                wxT("Maximum"),
                RegisterAddress::AlgorithmMaxValidMag,
                true, QValue::CalibratedMagnetometer);
        registerTree->AppendItem(
            algorithmValidField, data->getText(), -1, -1, data);
        // Algorithm Quarternion Tare
        wxTreeItemId algorithmTare = registerTree->AppendItem(
            algorithm, wxT("Quaternion Tare"));
        // Algorithm Quarternion Tare Element 0
        data = new RegisterTreeItemData(
                wxT("Element 0"),
                RegisterAddress::AlgorithmTareQuat0,
                true, QValue::Quaternion);
        registerTree->AppendItem(
            algorithmTare, data->getText(), -1, -1, data);
        // Algorithm Quarternion Tare Element 1
        data = new RegisterTreeItemData(
                wxT("Element 1"),
                RegisterAddress::AlgorithmTareQuat1,
                true, QValue::Quaternion);
        registerTree->AppendItem(
            algorithmTare, data->getText(), -1, -1, data);
        // Algorithm Quarternion Tare Element 2
        data = new RegisterTreeItemData(
                wxT("Element 2"),
                RegisterAddress::AlgorithmTareQuat2,
                true, QValue::Quaternion);
        registerTree->AppendItem(
            algorithmTare, data->getText(), -1, -1, data);
        // Algorithm Quarternion Tare Element 3
        data = new RegisterTreeItemData(
                wxT("Element 3"),
                RegisterAddress::AlgorithmTareQuat3,
                true, QValue::Quaternion);
        registerTree->AppendItem(
            algorithmTare, data->getText(), -1, -1, data);

        // Create Data Output Settings items.
        // Sensor Data Mode
        mapValue.Empty();
            mapValue.Add(wxT("Raw ADC results"));
            mapValue.Add(wxT("Calibrated measurements"));
        data = new RegisterTreeItemData(
                wxT("Sensor Data Mode"),
                RegisterAddress::SensorDataMode,
                mapValue);
        registerTree->AppendItem(
            output, data->getText(), -1, -1, data);
        // Date-Time Data Output Rate
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("1 Hz"));
            mapValue.Add(wxT("2 Hz"));
            mapValue.Add(wxT("4 Hz"));
            mapValue.Add(wxT("8 Hz"));
            mapValue.Add(wxT("16 Hz"));
            mapValue.Add(wxT("32 Hz"));
            mapValue.Add(wxT("64 Hz"));
            mapValue.Add(wxT("128 Hz"));
            mapValue.Add(wxT("256 Hz"));
            mapValue.Add(wxT("512 Hz"));
        data = new RegisterTreeItemData(
                wxT("Date-Time Data Rate"),
                RegisterAddress::DateTimeDataRate,
                mapValue);
        registerTree->AppendItem(
            output, data->getText(), -1, -1, data);
        // Battery and Thermometer Data Output Rate
        data = new RegisterTreeItemData(
                wxT("Battery and Thermometer Data Rate"),
                RegisterAddress::BatteryAndTemperatureDataRate,
                mapValue);
        registerTree->AppendItem(
            output, data->getText(), -1, -1, data);
        // Inertial and Magnetic Data Output Rate
        data = new RegisterTreeItemData(
                wxT("Inertial and Magnetic Data Rate"),
                RegisterAddress::InertialAndMagneticDataRate,
                mapValue);
        registerTree->AppendItem(
            output, data->getText(), -1, -1, data);
        // Quaternion Data Output Rate
        data = new RegisterTreeItemData(
                wxT("Quaternion Data Rate"),
                RegisterAddress::QuaternionDataRate,
                mapValue);
        registerTree->AppendItem(
            output, data->getText(), -1, -1, data);

        // Create SD Card items.
        // SD Card New Filename
        data = new RegisterTreeItemData(
                wxT("New Filename"),
                RegisterAddress::SDcardNewFileName,
                true);
        registerTree->AppendItem(
            sdCard, data->getText(), -1, -1, data);

        // Create Power Management items.
        // Battery Shutdown Voltage
        data = new RegisterTreeItemData(
                wxT("Battery Shutdown Voltage"),
                RegisterAddress::BatteryShutdownVoltage,
                true, QValue::CalibratedBattery);
        registerTree->AppendItem(
            power, data->getText(), -1, -1, data);
        // Sleep Timer
        data = new RegisterTreeItemData(
                wxT("Sleep Timer"),
                RegisterAddress::SleepTimer,
                true);
        registerTree->AppendItem(
            power, data->getText(), -1, -1, data);
        // Motion Trigger Wakeup
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("Low Sensitivity"));
            mapValue.Add(wxT("High Sensitivity"));
        data = new RegisterTreeItemData(
                wxT("Motion Trigger Wakeup"),
                RegisterAddress::MotionTrigWakeUp,
                mapValue);
        registerTree->AppendItem(
            power, data->getText(), -1, -1, data);
        // Bluetooth Power
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("Enabled"));
        data = new RegisterTreeItemData(
                wxT("Bluetooth Power"),
                RegisterAddress::BluetoothPower,
                mapValue);
        registerTree->AppendItem(
            power, data->getText(), -1, -1, data);

        // Create Auxillary Port items.
        // Auxiliary Port Mode
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("Digital I/O"));
            mapValue.Add(wxT("Analogue Input"));
            mapValue.Add(wxT("PWM Output"));
            mapValue.Add(wxT("ADXL345 Bus"));
            mapValue.Add(wxT("UART"));
            mapValue.Add(wxT("Sleep/Wake"));
        data = new RegisterTreeItemData(
                wxT("Mode"),
                RegisterAddress::AuxiliaryPortMode,
                mapValue);
        registerTree->AppendItem(
            auxillary, data->getText(), -1, -1, data);

        // Digital I/O
        wxTreeItemId digitalIO = registerTree->AppendItem(
            auxillary, wxT("Digital I/O"));
        // Digital I/O Direction
        mapValue.Empty();
            mapValue.Add(wxT("All channels are inputs"));
            mapValue.Add(wxT("Channels 0-6 are inputs, 7 is an output"));
            mapValue.Add(wxT("Channels 0-5 are inputs, 6-7 are outputs"));
            mapValue.Add(wxT("Channels 0-4 are inputs, 5-7 are outputs"));
            mapValue.Add(wxT("Channels 0-3 are inputs, 4-7 are outputs"));
            mapValue.Add(wxT("Channels 0-2 are inputs, 3-7 are outputs"));
            mapValue.Add(wxT("Channels 0-1 are inputs, 2-7 are outputs"));
            mapValue.Add(wxT("Channels 0 is an input, 1-7 are outputs"));
            mapValue.Add(wxT("All channels are outputs"));
        data = new RegisterTreeItemData(
                wxT("Direction"),
                RegisterAddress::DigitalIOdirection,
                mapValue);
        registerTree->AppendItem(
            digitalIO, data->getText(), -1, -1, data);
        // Digital I/O Data Output Rate
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("1 Hz"));
            mapValue.Add(wxT("2 Hz"));
            mapValue.Add(wxT("4 Hz"));
            mapValue.Add(wxT("8 Hz"));
            mapValue.Add(wxT("16 Hz"));
            mapValue.Add(wxT("32 Hz"));
            mapValue.Add(wxT("64 Hz"));
            mapValue.Add(wxT("128 Hz"));
            mapValue.Add(wxT("256 Hz"));
            mapValue.Add(wxT("512 Hz"));
        data = new RegisterTreeItemData(
                wxT("Data Output Rate"),
                RegisterAddress::DigitalIOdataRate,
                mapValue);
        registerTree->AppendItem(
            digitalIO, data->getText(), -1, -1, data);

        // Analogue Input
        wxTreeItemId analogueInput = registerTree->AppendItem(
            auxillary, wxT("Analogue Input Data"));
        // Analogue Input Data Mode
        mapValue.Empty();
            mapValue.Add(wxT("Raw ADC Results"));
            mapValue.Add(wxT("Calibrated measurements"));
        data = new RegisterTreeItemData(
                wxT("Data Mode"),
                RegisterAddress::AnalogueInputDataMode,
                mapValue);
        registerTree->AppendItem(
            analogueInput, data->getText(), -1, -1, data);
        // Analogue Input Data Output Rate
        mapValue.Empty();
            mapValue.Add(wxT("On change only"));
            mapValue.Add(wxT("1 Hz"));
            mapValue.Add(wxT("2 Hz"));
            mapValue.Add(wxT("4 Hz"));
            mapValue.Add(wxT("8 Hz"));
            mapValue.Add(wxT("16 Hz"));
            mapValue.Add(wxT("32 Hz"));
            mapValue.Add(wxT("64 Hz"));
            mapValue.Add(wxT("128 Hz"));
            mapValue.Add(wxT("256 Hz"));
            mapValue.Add(wxT("512 Hz"));
        data = new RegisterTreeItemData(
                wxT("Data Rate"),
                RegisterAddress::AnalogueInputDataRate,
                mapValue);
        registerTree->AppendItem(
            analogueInput, data->getText(), -1, -1, data);
        // Analogue Input Sensitivity
        data = new RegisterTreeItemData(
                wxT("Sensitivity"),
                RegisterAddress::AnalogueInputSensitivity,
                true, QValue::AnalogueInputSensitivity);
        registerTree->AppendItem(
            analogueInput, data->getText(), -1, -1, data);
        // Analogue Input Bias
        data = new RegisterTreeItemData(
                wxT("Bias"),
                RegisterAddress::AnalogueInputBias,
                true, QValue::AnalogueInputBias);
        registerTree->AppendItem(
            analogueInput, data->getText(), -1, -1, data);

        // PWM Frequency
        data = new RegisterTreeItemData(
                wxT("PWM Frequency"),
                RegisterAddress::PWMoutputFrequency,
                true);
        registerTree->AppendItem(
            auxillary, data->getText(), -1, -1, data);

        // ADXL345 Bus
        wxTreeItemId adxl345 = registerTree->AppendItem(
            auxillary, wxT("ADXL345 Bus"));
        // ADXL345 Bus Data Mode
        mapValue.Empty();
            mapValue.Add(wxT("Raw ADC Results"));
            mapValue.Add(wxT("Calibrated measurements"));
        data = new RegisterTreeItemData(
                wxT("Data Mode"),
                RegisterAddress::ADXL345busDataMode,
                mapValue);
        registerTree->AppendItem(
            adxl345, data->getText(), -1, -1, data);
        // ADXL345 Bus Data Output Rate
        mapValue.Empty();
            mapValue.Add(wxT("On change only"));
            mapValue.Add(wxT("1 Hz"));
            mapValue.Add(wxT("2 Hz"));
            mapValue.Add(wxT("4 Hz"));
            mapValue.Add(wxT("8 Hz"));
            mapValue.Add(wxT("16 Hz"));
            mapValue.Add(wxT("32 Hz"));
            mapValue.Add(wxT("64 Hz"));
            mapValue.Add(wxT("128 Hz"));
            mapValue.Add(wxT("256 Hz"));
            mapValue.Add(wxT("512 Hz"));
        data = new RegisterTreeItemData(
                wxT("Data Rate"),
                RegisterAddress::ADXL345busDataRate,
                mapValue);
        registerTree->AppendItem(
            adxl345, data->getText(), -1, -1, data);
        // ADXL345 Bus A
        wxTreeItemId adxl345A = registerTree->AppendItem(
            adxl345, wxT("A"));
        // ADXL345 Bus A Sensitivity
        wxTreeItemId adxl345ASensitivity = registerTree->AppendItem(
            adxl345A, wxT("Sensitivity"));
        // ADXL345 A X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345AsensitivityX,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345ASensitivity, data->getText(), -1, -1, data);
        // ADXL345 A Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345AsensitivityY,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345ASensitivity, data->getText(), -1, -1, data);
        // ADXL345 A Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345AsensitivityZ,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345ASensitivity, data->getText(), -1, -1, data);
        // ADXL345 Bus A Bias
        wxTreeItemId adxl345ABias = registerTree->AppendItem(
            adxl345A, wxT("Bias"));
        // ADXL345 A X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345AbiasX,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345ABias, data->getText(), -1, -1, data);
        // ADXL345 A Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345AbiasY,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345ABias, data->getText(), -1, -1, data);
        // ADXL345 A Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345AbiasZ,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345ABias, data->getText(), -1, -1, data);
        // ADXL345 Bus B
        wxTreeItemId adxl345B = registerTree->AppendItem(
            adxl345, wxT("B"));
        // ADXL345 Bus B Sensitivity
        wxTreeItemId adxl345BSensitivity = registerTree->AppendItem(
            adxl345B, wxT("Sensitivity"));
        // ADXL345 B X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345BsensitivityX,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345BSensitivity, data->getText(), -1, -1, data);
        // ADXL345 B Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345BsensitivityY,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345BSensitivity, data->getText(), -1, -1, data);
        // ADXL345 B Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345BsensitivityZ,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345BSensitivity, data->getText(), -1, -1, data);
        // ADXL345 Bus B Bias
        wxTreeItemId adxl345BBias = registerTree->AppendItem(
            adxl345B, wxT("Bias"));
        // ADXL345 B X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345BbiasX,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345BBias, data->getText(), -1, -1, data);
        // ADXL345 B Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345BbiasY,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345BBias, data->getText(), -1, -1, data);
        // ADXL345 B Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345BbiasZ,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345BBias, data->getText(), -1, -1, data);
        // ADXL345 Bus C
        wxTreeItemId adxl345C = registerTree->AppendItem(
            adxl345, wxT("C"));
        // ADXL345 Bus C Sensitivity
        wxTreeItemId adxl345CSensitivity = registerTree->AppendItem(
            adxl345C, wxT("Sensitivity"));
        // ADXL345 C X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345CsensitivityX,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345CSensitivity, data->getText(), -1, -1, data);
        // ADXL345 C Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345CsensitivityY,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345CSensitivity, data->getText(), -1, -1, data);
        // ADXL345 C Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345CsensitivityZ,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345CSensitivity, data->getText(), -1, -1, data);
        // ADXL345 Bus C Bias
        wxTreeItemId adxl345CBias = registerTree->AppendItem(
            adxl345C, wxT("Bias"));
        // ADXL345 C X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345CbiasX,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345CBias, data->getText(), -1, -1, data);
        // ADXL345 C Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345CbiasY,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345CBias, data->getText(), -1, -1, data);
        // ADXL345 C Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345CbiasZ,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345CBias, data->getText(), -1, -1, data);
        // ADXL345 Bus D
        wxTreeItemId adxl345D = registerTree->AppendItem(
            adxl345, wxT("D"));
        // ADXL345 Bus D Sensitivity
        wxTreeItemId adxl345DSensitivity = registerTree->AppendItem(
            adxl345D, wxT("Sensitivity"));
        // ADXL345 D X-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345DsensitivityX,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345DSensitivity, data->getText(), -1, -1, data);
        // ADXL345 D Y-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345DsensitivityY,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345DSensitivity, data->getText(), -1, -1, data);
        // ADXL345 D Z-Axis Sensitivity
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345DsensitivityZ,
                true, QValue::ADXL345busSensitivity);
        registerTree->AppendItem(
            adxl345DSensitivity, data->getText(), -1, -1, data);
        // ADXL345 Bus D Bias
        wxTreeItemId adxl345DBias = registerTree->AppendItem(
            adxl345D, wxT("Bias"));
        // ADXL345 D X-Axis Bias
        data = new RegisterTreeItemData(
                wxT("X-Axis"),
                RegisterAddress::ADXL345DbiasX,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345DBias, data->getText(), -1, -1, data);
        // ADXL345 D Y-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Y-Axis"),
                RegisterAddress::ADXL345DbiasY,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345DBias, data->getText(), -1, -1, data);
        // ADXL345 D Z-Axis Bias
        data = new RegisterTreeItemData(
                wxT("Z-Axis"),
                RegisterAddress::ADXL345DbiasZ,
                true, QValue::ADXL345busBias);
        registerTree->AppendItem(
            adxl345DBias, data->getText(), -1, -1, data);

        // UART
        wxTreeItemId uart = registerTree->AppendItem(
            auxillary, wxT("UART"));
        // UART Baud Rate
        mapValue.Empty();
            mapValue.Add(wxT("2400"));
            mapValue.Add(wxT("4800"));
            mapValue.Add(wxT("7200"));
            mapValue.Add(wxT("9600"));
            mapValue.Add(wxT("14400"));
            mapValue.Add(wxT("19200"));
            mapValue.Add(wxT("38400"));
            mapValue.Add(wxT("57600"));
            mapValue.Add(wxT("115200"));
            mapValue.Add(wxT("230400"));
            mapValue.Add(wxT("460800"));
            mapValue.Add(wxT("921600"));
        data = new RegisterTreeItemData(
                wxT("Baud Rate"),
                RegisterAddress::UARTbaudRate,
                mapValue, wxT("baud"));
        registerTree->AppendItem(
            uart, data->getText(), -1, -1, data);
        // UART UART Hardware Flow Control
        mapValue.Empty();
            mapValue.Add(wxT("Disabled"));
            mapValue.Add(wxT("Enabled"));
        data = new RegisterTreeItemData(
                wxT("Hardware Flow Control"),
                RegisterAddress::UARThardwareFlowControl,
                mapValue);
        registerTree->AppendItem(
            uart, data->getText(), -1, -1, data);
    }

    wxTreeItemId GUIFrame::getRegisterTreeItem(
        wxTreeItemId rootItem, RegisterAddress address)
    {
        wxTreeItemIdValue cookie;
        wxTreeItemId child = registerTree->GetFirstChild(rootItem, cookie);
        while (child.IsOk())
        {
            wxTreeItemData* data = registerTree->GetItemData(child);
            if (data != 0)
            {
                RegisterTreeItemData* registerData =
                    (RegisterTreeItemData*)data;
                if (registerData->getAddress() == address)
                {
                    return child;
                }
            }
            wxTreeItemId grandChild = getRegisterTreeItem(child, address);
            if (grandChild.IsOk())
                {
                    return grandChild;
                }
            child = registerTree->GetNextChild(rootItem, cookie);
        }
        return child;
    }

    void GUIFrame::treeItemExpanded(wxTreeEvent &event)
    {
        wxTreeItemId item = event.GetItem();
        wxTreeItemIdValue cookie;
        wxTreeItemId child = registerTree->GetFirstChild(item, cookie);
        while (child.IsOk())
        {
            wxTreeItemData* data = registerTree->GetItemData(child);
            if (data != 0)
            {
                RegisterTreeItemData* registerData =
                    (RegisterTreeItemData*)data;
                api->sendReadRegister(registerData->getAddress());
            }
            child = registerTree->GetNextChild(item, cookie);
        }
    }

    void GUIFrame::treeItemRightClick(wxTreeEvent &event)
    {
        wxTreeItemId item = event.GetItem();
        wxTreeItemData* data = registerTree->GetItemData(item);
        wxMenu rightClickMenu;
        rightClickMenu.SetClientData(&event);
        if (data != 0)
        {
            RegisterTreeItemData* registerData = (RegisterTreeItemData*)data;
            if (registerData->isEditable())
            {
                rightClickMenu.Append(ID_EDIT, "Edit");
            }
            rightClickMenu.Append(ID_REFRESH, "Refresh");
        }
        else
        {
            rightClickMenu.Append(ID_REFRESH_ALL, "Refresh All");
        }
        rightClickMenu.Bind(wxEVT_COMMAND_MENU_SELECTED,
                rightClickMenuClick);
        PopupMenu(&rightClickMenu);
    }

    void GUIFrame::rightClickMenuClick(wxCommandEvent &event)
    {
        wxMenu* menu = (wxMenu*)event.GetEventObject();
        wxTreeEvent* treeEvent = (wxTreeEvent*)menu->GetClientData();
        wxTreeItemId item = treeEvent->GetItem();
        wxTreeCtrl* registerTree = (wxTreeCtrl*)treeEvent->GetEventObject();
        wxWindow* parent = registerTree->GetParent();
        while (parent->GetParent() != 0)
            parent = parent->GetParent();
        GUIFrame* frame = (GUIFrame*)parent;
        wxTreeItemData* data = frame->registerTree->GetItemData(item);
        RegisterTreeItemData* registerData = 0;
        if (data != 0) registerData = (RegisterTreeItemData*) data;

        switch (event.GetId())
        {
        case ID_EDIT:
            if (registerData != 0)
            {
                unsigned short newValue;
                EditDialog editDialog(frame, registerData, &newValue);
                if (editDialog.ShowModal() == wxID_OK)
                {
                    frame->api->sendWriteRegister(registerData->getAddress(),
                        newValue);
                }
            }
            break;
        case ID_REFRESH:
            if (registerData != 0)
            {
                frame->api->sendReadRegister(registerData->getAddress());
                frame->statusBar->SetStatusText("Refreshing...");
            }
            break;
        case ID_REFRESH_ALL:
            wxTreeItemId current = item;
            do
            {
                if (registerData != 0)
                    frame->api->sendReadRegister(registerData->getAddress());
                // If item has children, get its first child.
                wxTreeItemIdValue cookie;
                wxTreeItemId child = registerTree->GetFirstChild(current, cookie);
                if (child.IsOk()) current = child;
                else
                {
                    // Else, while not the original item
                    while (current != item)
                    {
                        // Check for siblings.
                        wxTreeItemId sibling = registerTree->GetNextSibling(current);
                        if (sibling.IsOk())
                        {
                            current = sibling;
                            break;
                        }
                        // And parent's siblings.
                        current = registerTree->GetItemParent(current);
                    }
                }
                data = frame->registerTree->GetItemData(current);
                if (data != 0) registerData = (RegisterTreeItemData*) data;
                else registerData = 0;
            } while (current != item);
            frame->statusBar->SetStatusText("Refreshing...");
            break;
        }
    }

    RegisterTreeItemData::RegisterTreeItemData(
        wxString name, RegisterAddress address,
        bool editable, QValue qValue, wxString unit, bool hexValue) :
        name(name), address(address),
        valueSet(false), editable(editable),
        qValue(qValue), mapped(false), unit(unit), hexValue(hexValue)
    {
    }

    RegisterTreeItemData::RegisterTreeItemData(
        wxString name, RegisterAddress address,
        wxArrayString mapValue, wxString unit) :
        name(name), address(address),
        valueSet(false), editable(true), qValue((QValue)0),
        mapped(true), mapValue(mapValue),
        unit(unit), hexValue(false)
    {
    }

    RegisterTreeItemData::~RegisterTreeItemData()
    {
    }

    wxString RegisterTreeItemData::getName()
    {
        return name;
    }

    RegisterAddress RegisterTreeItemData::getAddress()
    {
        return address;
    }

    bool RegisterTreeItemData::isValueSet()
    {
        return valueSet;
    }

    float RegisterTreeItemData::getValue()
    {
        if (valueSet)
            return value;
        else
            return 0;
    }

    void RegisterTreeItemData::setValue(float value)
    {
        this->value = value;
        valueSet = true;
    }

    bool RegisterTreeItemData::isEditable()
    {
        return editable;
    }

    QValue RegisterTreeItemData::getQValue()
    {
        return qValue;
    }

    bool RegisterTreeItemData::isMapped()
    {
        return mapped;
    }

    wxArrayString RegisterTreeItemData::getMapValue()
    {
        return mapValue;
    }

    wxString RegisterTreeItemData::getText()
    {
        wxString text = name;
        text << ": ";
        if (valueSet)
        {
            if (mapped)
            {
                text << mapValue[(short)value];
            }
            else if (hexValue)
            {
                std::stringstream s;
                s << std::hex << (short)value;
                text << s.str();
            }
            else if (qValue == (QValue)0)
            {
                text << (short)value;
            }
            else
            {
                text << value;
            }
        }
        else text << '?';
        text << unit;
        return text;
    }

    EditDialog::EditDialog(wxWindow* parent,
            RegisterTreeItemData* registerData, unsigned short* newValue) :
        wxDialog(parent, wxID_ANY, wxEmptyString),
 		registerData(registerData), newValue(newValue)
    {
        this->SetSizeHints( wxDefaultSize, wxDefaultSize );

        wxBoxSizer* editDialogSizer;
        editDialogSizer = new wxBoxSizer( wxVERTICAL );

        editLabel = new wxStaticText( this, wxID_ANY,
                wxT("Please enter the new register value:"), wxDefaultPosition,
                wxDefaultSize, 0 );
        editLabel->Wrap( -1 );
        editDialogSizer->Add( editLabel, 0, wxALL, 5 );

        editValue = new wxTextCtrl( this, wxID_ANY, wxEmptyString,
                wxDefaultPosition, wxDefaultSize, 0 );
        editDialogSizer->Add( editValue, 0, wxALL, 5 );

        wxArrayString editChoiceChoices;
        editChoice = new wxChoice( this, wxID_ANY, wxDefaultPosition,
                wxDefaultSize, editChoiceChoices, 0 );
        editChoice->SetSelection( 0 );
        editDialogSizer->Add( editChoice, 0, wxALL, 5 );

        editDialogButton = new wxStdDialogButtonSizer();
        editDialogButtonOK = new wxButton( this, wxID_OK );
        editDialogButton->AddButton( editDialogButtonOK );
        editDialogButtonCancel = new wxButton( this, wxID_CANCEL );
        editDialogButton->AddButton( editDialogButtonCancel );
        editDialogButton->Realize();
        editDialogSizer->Add( editDialogButton, 1, wxEXPAND, 5 );

        this->SetSizer( editDialogSizer );
        this->Layout();
        editDialogSizer->Fit( this );

        // Bind Events
        editDialogButtonOK->Bind(wxEVT_COMMAND_BUTTON_CLICKED,
                &EditDialog::okButtonPressed, this);

        wxString title = wxT("Edit Register");
        title << registerData->getName();
        this->SetLabel(title);
        if (registerData->isMapped())
        {
            editDialogSizer->Detach(editValue);
            editValue->Destroy();
            editChoice->Append(registerData->getMapValue());
            if (registerData->isValueSet())
            {
                editChoice->SetSelection((int)registerData->getValue());
            }
            editDialogSizer->Layout();
        }
        else
        {
            editDialogSizer->Detach(editChoice);
            editChoice->Destroy();
            editDialogSizer->Layout();
            float maxValue = getMaxFloat(registerData->getQValue());
            wxString label = editLabel->GetLabelText();
            label << "\n(between " << -maxValue << " and " << maxValue << ")";
            editLabel->SetLabel(label);
        }
    }

    EditDialog::~EditDialog()
    {
        // Unbind Events
        editDialogButtonOK->Unbind(wxEVT_COMMAND_BUTTON_CLICKED,
                &EditDialog::okButtonPressed, this);
    }

    float EditDialog::getMaxFloat(QValue qValue)
    {
        return (float)(1 << (15 - (int)qValue));
    }

    void EditDialog::okButtonPressed(wxCommandEvent &event)
    {
        if (registerData->isMapped())
        {
            int choice = editChoice->GetSelection();
            if (choice == wxNOT_FOUND)
            {
                // Nothing selected error.
                wxMessageDialog error(this, wxT("Nothing selected."),
                        wxT("Error"), wxICON_ERROR);
                error.ShowModal();
            }
            else
            {
                *newValue = (unsigned short)choice;
                event.Skip();
            }
        }
        else
        {
            QValue qValue = registerData->getQValue();
            wxString valueText = editValue->GetValue();
            double valueDouble;
            float maxValue = getMaxFloat(qValue);
            if(!valueText.ToDouble(&valueDouble))
            {
                // Not a number error.
                wxMessageDialog error(this, wxT("Not a number."),
                        wxT("Error"), wxICON_ERROR);
                error.ShowModal();
            }
            else if(valueDouble > maxValue || valueDouble < -maxValue)
            {
                // Not within the range error.
                wxMessageDialog error(this, wxT("Not within the range."),
                        wxT("Error"), wxICON_ERROR);
                error.ShowModal();
            }
            else
            {
                *newValue = floatToShort(valueDouble, qValue);
                event.Skip();
            }
        }
    }

    GraphFrame::GraphFrame(wxWindow* parent, API* api, const wxString& title) :
        wxFrame(parent, wxID_ANY, title), api(api), listener(0)
    {
    }

    GraphFrame::~GraphFrame()
    {
        if (listener != 0) api->removeListener(listener);
    }

    VoltageGraphFrame::VoltageGraphFrame(wxWindow* parent, API* api) :
        GraphFrame(parent, api, wxString("Voltage Graph"))
    {
        ma::OscilloscopePanel* oscilloscope = new ma::OscilloscopePanel(this);
        listener = new VoltageDataListener(oscilloscope);
        api->addListener(listener);
    }

    TemperatureGraphFrame::TemperatureGraphFrame(wxWindow* parent, API* api) :
        GraphFrame(parent, api, wxString("Thermometer Graph"))
    {
        ma::OscilloscopePanel* oscilloscope = new ma::OscilloscopePanel(this);
        listener = new TemperatureDataListener(oscilloscope);
        api->addListener(listener);
    }

    GyroscopeGraphFrame::GyroscopeGraphFrame(wxWindow* parent, API* api) :
        GraphFrame(parent, api, wxString("Gyroscope Graph"))
    {
        ma::OscilloscopePanel* oscilloscope = new ma::OscilloscopePanel(this, 3);
        listener = new GyroscopeDataListener(oscilloscope);
        api->addListener(listener);
    }

    AccelerometerGraphFrame::AccelerometerGraphFrame(wxWindow* parent, API* api) :
        GraphFrame(parent, api, wxString("Accelerometer Graph"))
    {
        ma::OscilloscopePanel* oscilloscope = new ma::OscilloscopePanel(this, 3);
        listener = new AccelerometerDataListener(oscilloscope);
        api->addListener(listener);
    }

    MagnetometerGraphFrame::MagnetometerGraphFrame(wxWindow* parent, API* api) :
        GraphFrame(parent, api, wxString("Magneteometer Graph"))
    {
        ma::OscilloscopePanel* oscilloscope = new ma::OscilloscopePanel(this, 3);
        listener = new MagnetometerDataListener(oscilloscope);
        api->addListener(listener);
    }

    CuboidFrame::CuboidFrame(wxWindow* parent, API* api) :
        GraphFrame(parent, api, wxString("3D Cuboid"))
    {
        ma::CuboidPanel* cuboidPanel = new ma::CuboidPanel(this);
        listener = new QuaternionDataListener(cuboidPanel);
        api->addListener(listener);
    }
}
